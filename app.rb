require 'sinatra'
#require 'sinatra/reloader'

get '/' do
  "<html>
    <head>
      <title>Linux Networking Code</title>
      <style>
        html { display: table; height: 100%; margin: 0 auto; }
        body { height: 100%; display: table-cell; vertical-align: center; }
        #box {
          padding: 10px;
        }
        input[type=text] {
          font-size: 1.2em;
          width: 400px;
        }
        button {
          font-size: 1.2em;
        }
      </style>
    </head>
    <body>
      <div id='search'>
        <div id='box'>
          <form action='/q' method='GET'>
            <input type='text' id='q' name='q' placeholder='function name or prefix' />
            <button type=submit>Go!</button>
          </form>
        </div>
      </div>
    </body>
   </html>"
end



get '/q' do
  q = params[:q]
  redirect "/path?q=#{q}" if q.include? "path:"
  q << ".*" unless q.include? ".*"
  t = TCPSocket.new 'localhost', 9999
  t.puts q
  n = t.readline.to_i
  puts n
  ret = []
  1.upto(n) { ret << t.readline.strip }
  t.close
  list = ret.map { |e| "<li><a href='/path?q=path:#{e}'>#{e}</a></li>" }.join("\n")
  "<html>
    <head>
      <title>Linux Networking Code - #{q}</title>
      <style>
        html { display: table; height: 100%; margin: 0 auto; }
        body { height: 100%; display: table-cell; vertical-align: center; }
        #box {
          padding: 10px;
        }
        input[type=text] {
          font-size: 1.2em;
          width: 400px;
        }
        button {
          font-size: 1.2em;
        }
      </style>
    </head>
    <body>
      <div id='search'>
        <div id='box'>
          <form action='/q' method='GET'>
            <input type='text' id='q' name='q' placeholder='function name or prefix' value='#{q}' />
            <button type=submit>Go!</button>
          </form>
        </div>
        <div id=results>
          #{list}
        </div>
      </div>
    </body>
   </html>"
end

get '/path' do
  q = params[:q]
  t = TCPSocket.new 'localhost', 9999
  t.puts q
  ret = []
  while true
    e = t.readline
    break if e.strip == '-1'
    ret << e.strip
  end
  t.close

  list = ret.map { |p|
    path = p.split(',').map { |e|
      "<a href='/q?q=#{e}'>#{e}</a>"
    }.join("&nbsp; - &nbsp;")
    "<li>#{path}</li>"
  }.join("\n")

  "<html>
    <head>
      <title>Linux Networking Code - #{q}</title>
      <style>
        html { display: table; height: 100%; margin: 0 auto; }
        body { height: 100%; display: table-cell; vertical-align: center; }
        #box {
          padding: 10px;
        }
        input[type=text] {
          font-size: 1.2em;
          width: 400px;
        }
        button {
          font-size: 1.2em;
        }
      </style>
    </head>
    <body>
      <div id='search'>
        <div id='box'>
          <form action='/q' method='GET'>
            <input type='text' id='q' name='q' placeholder='function name or prefix' value='#{q}' />
            <button type=submit>Go!</button>
          </form>
        </div>
        <div id=results>
          #{list}
        </div>
      </div>
    </body>
   </html>"
end
